;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; setup:

(setq mm-setup-allow-duplication 0)
(setq mm-setup-number-of-colors 5)
(setq mm-setup-number-of-holes 4)
(setq mm-constant-alphabeth-list '("a" "b" "c" "d" "e" "f" "g" "h" "i" "j" "k" "l" "m" "n" "o" "p" "r" "s" "t" "u" "v" "q" "w" "x" "y" "z"))
(setq mm-constant-max-time 1000)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun final-answer (&optional answer)
  "Returns answer containing 1 with length equal to mm-setup-number-of-holes."
  (if (<= mm-setup-number-of-holes (length answer)) answer
    (final-answer (concat "1" answer))))

;; test examples:
(setq mm-setup-number-of-holes 4)
(equal (final-answer) "1111")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun string-to-list (string)
  "Converts input string to list containing letters from the input string."
  (recursive-string-to-list string nil)
  )

(defun recursive-string-to-list (string list)
  "Recursively converts input string to list containing letters from the input string."
  (if (= 0 (length string)) list
    (recursive-string-to-list (substring string 1 (length string)) (append list (list (substring string 0 1))))))

;; test examples:
(equal (string-to-list "abcdef") '("a" "b" "c" "d" "e" "f"))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun list-to-string (list)
  "Concatenates lists elements to produce single string."
  (recursive-list-to-string list ""))

(defun recursive-list-to-string (list string)
  "Recursively concatenates lists elements to produce single string."
  (if (null list) string
      (recursive-list-to-string (cdr list) (concat string (car list)))))

;; test examples:
(equal (list-to-string '("1" "2" "3" "4")) "1234")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun validate-code (list)
  "Validates user player answer passed as a list - returns 't if valid of 'nil if not valid."
  (if (= mm-setup-number-of-holes (length list))
      (recursive-validate-list (string-to-list list) nil) nil))

(defun recursive-validate-list (list processed)
  "Recursively validates user player answer"
  (if (null list) t
      (if (not (member (car list) mm-constant-alphabeth-list)) nil
	(if (>= (position (car list) mm-constant-alphabeth-list :test #'equal)  mm-setup-number-of-colors) nil
	  (if (and (member (car list) processed) (= 0 mm-setup-allow-duplication)) nil
	    (recursive-validate-list (cdr list) (append processed (list (car list)))))))))

;; test examples:
(setq mm-setup-allow-duplication 1)
(setq mm-setup-number-of-colors 5)
(setq mm-setup-number-of-holes 4)
(equal (validate-code "abcd") t)
(equal (validate-code "abdd") t)
(equal (validate-code "abdd") t)
(equal (validate-code "abdg") nil)
(equal (validate-code "") nil)
(equal (validate-code "eeee") t)
(equal (validate-code "eeef") nil)

(setq mm-setup-allow-duplication 0)
(setq mm-setup-number-of-colors 5)
(setq mm-setup-number-of-holes 4)
(equal (validate-code "abcd") t)
(equal (validate-code "abdd") nil)
(equal (validate-code "abda") nil)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun check-good (guess code)
  "Returns amount of correct colors on correct places and the remaining rests."
  (check-good-recursively guess code nil nil nil))

(defun check-good-recursively (guess code answer rest-guess rest-code)
  "Recusrively returns amount of correct colors on correct places and the remaining rests."
  (if (null guess) (list answer rest-guess rest-code)
    (if (equal (car guess) (car code))
	(check-good-recursively (cdr guess) (cdr code) (append answer (list "1")) rest-guess rest-code)
      (check-good-recursively (cdr guess) (cdr code) answer (append rest-guess (list (car guess))) (append rest-code (list (car code))))
      )))

;; test examples:
(equal (check-good '(1 2 3 4 5 1 2) '(1 2 3 3 5 9 8)) '(("1" "1" "1" "1") (4 1 2) (3 9 8)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun check-bad (corrects guess code)
  "Returns amount of badly placed colors in guess."
  (append corrects (check-bad-recursively guess code nil)))

(defun check-bad-recursively (guess code answer)
  "Recursively returns amount of badly placed colors in guess."
  (if (null guess) answer
    (if (member (car guess) code)
	(check-bad-recursively (cdr guess)
			       (append (subseq code 0 (position (car guess) code :test #'equal))
				       (subseq code (1+ (position (car guess) code :test #'equal)) (length code)))
			       (append answer (list "0")))
      (check-bad-recursively (cdr guess) code answer))))

;; test examples:
(equal (check-bad '() '(1 2 3) '(3 1 4)) '("0" "0"))
(equal (check-bad '("1") '(1 2 3) '(3 1 4)) '("1" "0" "0"))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun get-answer (guess code)
  "Returns answer for passed guess and code."
  (apply 'check-bad (check-good guess code)))

(defun answer (guess code)
  "Returns answer for passed guess and code."
  (list-to-string (get-answer (string-to-list guess) (string-to-list code))))

;; test examples:
(equal (answer "abc"  "ddd") "")
(equal (answer "abc"  "aaa") "1")
(equal (answer "abb"  "baa") "00")
(equal (answer "abc"  "acc") "11")
(equal (answer "abc"  "acb") "100")
(equal (answer "abc"  "abc") "111")
(equal (answer "bbbok"  "obkok") "111")
(equal (answer "bbbob"  "baaob") "111")
(equal (answer "sssok"  "seksi") "100")
(equal (answer "zakop"  "ppokz") "0000")
(equal (answer "aab" "baa") "100")
(equal (answer "nnogao"  "onngao") "111100")
(equal (answer "aabboodo"  "ababoood") "11110000")
(equal (answer "xyzxyz"  "axxxza") "100")
(equal (answer "cbdddc"  "dddccc") "11000")
(equal (answer "bbaodo" "aaodoo") "1000")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun sort-answer (answer)
  "Sorts answer elements."
  (list-to-string (sort (string-to-list answer) 'string>)))

;; test examples:
(equal (sort-answer "01-1-00-1") "111000---")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun is-not-contradictory (new old answer)
  "Checks contradictoriness of new guess, when answer for old one is known. "
  (equal (sort-answer (answer new old)) (sort-answer answer)))

;; test examples:
(equal (is-not-contradictory "abc" "cab" "10") nil)
(equal (is-not-contradictory "abc" "aab" "10") t)
(equal (is-not-contradictory "bbaodo" "aaodoo" "1000") t)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun validate-answer (answer)
  "Validates answer"
  (validate-answer-recursively answer 0))

(defun validate-answer-recursively (answer counter)
  "Recursively validates answer"
  (if (null answer) (<= counter mm-setup-number-of-holes)
    (if (or (equal "1" (car answer)) (equal "0" (car answer)))
	(validate-answer-recursively (cdr answer) (1+ counter))
      nil)))

;; test examples:
(setq mm-setup-number-of-holes 4)
(equal (validate-answer '("0" "0" "a")) nil)
(equal (validate-answer '("0" "0" "1" "1" "0")) nil)
(equal (validate-answer '("0" "z")) nil)
(equal (validate-answer '()) t)
(equal (validate-answer '("1" "1" "1" "1")) t)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun guess-fits-knowledgebase (guess knowledgebase)
  "Checks if guess is not contradictory with some knowledgebase entries"
  (if (null knowledgebase) t
    (if (is-not-contradictory guess (nth 0 (car knowledgebase)) (nth 1 (car knowledgebase)))
	(guess-fits-knowledgebase guess (cdr knowledgebase))
      nil)))

;; test examples:
(equal (guess-fits-knowledgebase "abcd" '(("abcd" "1111"))) t)
(equal (guess-fits-knowledgebase "abcc" '(("abcd" "1111"))) nil)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun mm-codemaker ()
  "Computer player mode - the LISP program is guessing the user player secret code."
  (interactive)
  (message "I am guessing your secret code. Number of holes: %d, number of colors: %d, duplication allowed: %d"
	   mm-setup-number-of-holes
	   mm-setup-number-of-colors
	   mm-setup-allow-duplication)
  (setq knowledgebase nil)
  (loop
   (setq times 0)
   (loop
    (setq times (1+ times))
    (if (> times mm-constant-max-time)
	(if (equal "Yes" (read-string "Are you sure you didn't make mistake? Type 'Yes' to confirm: "))
	    (setq times 0)
	  (error "User put incorrect answer!")))
    (setq guess (get-random-code))
    (if (guess-fits-knowledgebase guess knowledgebase) (return)))
   (loop
    (setq answer (string-to-list (read-string (format "Is %s correct? " guess))))
    (if (validate-answer answer) (return) (read-string "Please enter correct answer. Press ENTER to continue.")))
   (if (equal (list-to-string answer) (final-answer)) (return (message "Secret code found!"))
     (progn
       (setq knowledgebase (append (list (list guess (list-to-string answer))) knowledgebase))
       (message "Your answer for guess: %s is: %s" guess answer)))))


;; interactive test:
;; (mm-codemaker)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun mm-codebreaker ()
  "Starts new game where user player guess the computer secret code."
  (interactive)
  (setq code (get-random-code))
  (message "New code generated. Press ENTER to start guessing. Press C-G to quite the game.")
  (loop
   (setq guess (read-string "Enter your guess: "))
   (if (validate-code guess)
       (progn
	 (setq answer (answer guess code))
	 (if (equal answer (final-answer)) (return (message "Congratulations! Secret code found!"))
	   (message "The answer for your guess: %s is: %s" guess answer)))
     (read-string "Please enter valid guess. Press ENTER to continue."))))

;; interactive test:
;; (mm-codebreaker)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun numbers-list (n)
  "Generates n integers list starting from 0"
  (if (= 1 n) (list 0)
    (append (numbers-list (1- n)) (list (1- n)))))

;; test examples:
(equal (numbers-list 4) '(0 1 2 3))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun get-random-code ()
  "Returns random code according to setup configuration number of holes, colors and duplication allowance."
  (if (and (= mm-setup-allow-duplication 0) (> mm-setup-number-of-holes mm-setup-number-of-colors))
      (error "ERROR: Number of holes > number of colors while duplication not allowed!"))
  (list-to-string (get-random-code-recursively (numbers-list mm-setup-number-of-colors) nil)))

(defun get-random-code-recursively (indexes code)
  "Recursively returns random code according to setup configuration number of holes, colors and duplication allowance."
  (if (= (length code) mm-setup-number-of-holes) code
    (progn
      (setq i (random (length indexes)))
      (if (= 0 mm-setup-allow-duplication)
	  (get-random-code-recursively (append (subseq indexes 0 i) (subseq indexes (1+ i) (length indexes)))
				       (append code (list (nth (nth i indexes) mm-constant-alphabeth-list))))
	(get-random-code-recursively indexes (append code (list (nth (nth i indexes) mm-constant-alphabeth-list))))))))

;; interactive test:
(setq mm-setup-allow-duplication 0)
(setq mm-setup-number-of-colors 5)
(setq mm-setup-number-of-holes 4)
(get-random-code)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
